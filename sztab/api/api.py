import json
import logging
import math
import time
import traceback
from dataclasses import dataclass
from threading import Thread

import uvicorn
from fastapi import FastAPI, File, Form, UploadFile

from sztab.mongo import messages as MongoMessages
from sztab.mongo.connection import MongoConnection
from sztab.mongo.mongo_definitions import MongoMessage, MongoMsgType
from sztab.robur import PhotoForRobur, RoburQueue

from .models import CommandForDrone, CommandType

logger = logging.getLogger(__name__)


@dataclass
class DroneTelemData:
    lat: float
    lon: float
    alt: float
    finished_command_timestamp: str


def default_image_callback(*_args, **_kwargs):
    logger.error("Default image callback called, use the callback bro")


class DroniadaApi:
    def __init__(
        self,
        robur_queue: RoburQueue,
        drone_telem_data: DroneTelemData,
        mongo_connection: MongoConnection,
    ):
        self.current_command_for_drone = CommandForDrone()
        self.app = FastAPI(
            description="Gwara ochweśnicka, zwana też kminą ochweśnicką – język tajny wspólnoty "
            "handlarzy i oszustów, powstały w połowie XIX wieku w Skulsku i Ślesinie. "
        )
        self.robur_queue = robur_queue
        self.drone_telem_data = drone_telem_data
        self.mongo_connection = mongo_connection

        # Define the handlers that will have access to ``self``
        @self.app.get("/")
        def read_root():
            return {"response": "obudziłem?"}

        @self.app.get("/drone")
        def command_for_drone():
            return self.current_command_for_drone

        @self.app.post("/drone")
        def override_command_for_drone(command: CommandForDrone):
            if command.command_type == CommandType.TAKEOFF:
                self.mongo_send_start_mission()

            self.current_command_for_drone = command
            return command

        @self.app.post("/image")
        def upload_image(
            file: UploadFile = File(...),
            lat: float = Form(...),
            lon: float = Form(...),
            alt: float = Form(...),
            heading: float = Form(...),
            pitch: float = Form(...),
            yaw: float = Form(...),
            roll: float = Form(...),
        ):
            robur_queue.put(
                PhotoForRobur(
                    frame=file,
                    lat=lat,
                    lon=lon,
                    alt=alt,
                    heading=heading,
                    pitch=pitch,
                    yaw=yaw,
                    roll=roll,
                )
            )
            return {
                "file_name": file.filename,
                "lat": lat,
                "lon": lon,
                "alt": alt,
                "heading": heading,
                "pitch": pitch,
                "yaw": yaw,
                "roll": roll,
                "file_content_type": file.content_type,
            }

        @self.app.post("/telem")
        def post_telem(data: dict):
            """
            Any json data sent here will be printed to stdout and therefore forwarded to ElasticSearch
            """
            if "drone_position" in data:
                self.drone_telem_data.lon, self.drone_telem_data.lat = data[
                    "drone_position"
                ]
                self.drone_telem_data.alt = data["altitude"]
                self.drone_telem_data.finished_command_timestamp = data[
                    "finished_command_timestamp"
                ]

                msg_dict_gps = MongoMessages.gps_rpy(
                    vehicle_type="uav",
                    vehicle_id=1,
                    gps_latitude=float(self.drone_telem_data.lat),
                    gps_longitude=float(self.drone_telem_data.lon),
                    gps_altitude=float(self.drone_telem_data.alt),
                    roll=float(data["attitude"]["roll"]),
                    pitch=float(data["attitude"]["pitch"]),
                    yaw=float(data["attitude"]["yaw"]),
                )
                gps_message = MongoMessage(
                    message_type=MongoMsgType.GPS_RPY, msg_content_dict=msg_dict_gps
                )
                mongo_connection.send_message(message=gps_message)

            print(json.dumps(data))

    def mongo_send_start_mission(self):
        logger.critical("Taking off and starting mission")
        mission_start_dict = MongoMessages.start()
        self.mongo_connection.send_message(
            MongoMessage(
                message_type=MongoMsgType.START, msg_content_dict=mission_start_dict
            )
        )


def fastapi_thread_function(app_to_run: FastAPI):
    while True:
        try:
            time.sleep(1)
            logging.critical("Starting the API")
            uvicorn.run(app_to_run, host="0.0.0.0", port=9998)
        except Exception:  # pylint: disable=broad-except
            logging.critical("FastAPI raised an exception: %s", traceback.format_exc())


def run_api(
    queue: RoburQueue,
    drone_telem_data: DroneTelemData,
    mongo_connection: MongoConnection,
) -> DroniadaApi:
    api = DroniadaApi(queue, drone_telem_data, mongo_connection=mongo_connection)

    fastapi_thread = Thread(target=fastapi_thread_function, args=(api.app,))
    fastapi_thread.start()

    return api
