#!/bin/bash

# Type checking is done by mypy
# https://github.com/python/mypy
# currently using the default settings,
# although there are some extra strict
# checks that can be enabled in the future
# Refer to `mypy --help`

set -Eeuo pipefail

poetry run mypy --namespace-packages sztab
