import logging
import os
import time
from queue import Queue

from .api import DroneTelemData, run_api
from .constants import (
    three_colors_kakolewo,
    three_colors_kakolewo_robur,
    three_colors_morskie_oko_robur,
    tree_of_life_kakolewo,
    tree_of_life_przed_hangarem,
)
from .missions.three_colours import run_three_colors
from .missions.tree_of_life import run_tree_of_life
from .mongo import MongoConnection
from .robur import DetectionsQueue, Robur, RoburQueue
from .robur_mock import RoburMock

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s,%(msecs)d %(levelname)s <%(threadName)s> [%(filename)s:%(lineno)d] %(message)s",
)

logger = logging.getLogger(__name__)

THREE_COLORS_SCAN_PATH = three_colors_kakolewo
# TREE_OF_LIFE_SCAN_PATH = tree_of_life_przed_hangarem
TREE_OF_LIFE_SCAN_PATH = tree_of_life_kakolewo


drone_telem_data = DroneTelemData(0, 0, 0, "UNKNOWN")
images_queue: RoburQueue = Queue()
detections_queue: DetectionsQueue = Queue()


mongo_connection = MongoConnection(
    os.environ["MONGO_USER"], os.environ["MONGO_PASSWORD"]
)

mongo_connection.start_mongo_thread()
api = run_api(images_queue, drone_telem_data, mongo_connection)

robur = Robur(images_queue, detections_queue)
# robur = RoburMock(detections_queue, drone_telem_data, tree_of_life_kakolewo, sad=False)

robur.run()


if "MISSION_3COLORS" in os.environ:
    logger.critical("Running three colors")
    run_three_colors(THREE_COLORS_SCAN_PATH, drone_telem_data, detections_queue, api)

elif "MISSION_TREE_OF_LIFE" in os.environ:
    logger.critical("Running tree of life")
    run_tree_of_life(TREE_OF_LIFE_SCAN_PATH, drone_telem_data, detections_queue, api)

else:
    while True:
        logger.critical("INVALID MISSION SELECTED, GO FIX THIS")
        time.sleep(1)
