import time
from queue import Queue

from geopy import distance

from sztab.api import DroneTelemData
from sztab.api.models import ScanCoordinate
from sztab.robur import DetectionsQueue
from sztab.robur_definitions import Detection
from sztab.robur_mock import RoburMock, Shape


def distance_between_gps_points(p1: Detection, p2: DroneTelemData) -> float:
    """
    distance in meters
    """
    return float(distance.distance([p1.lat, p1.lon], [p2.lat, p2.lon]).meters)


# TODO: fix new sad generation
# def test_generate_shapes_list():
#     scan_polygon = [
#         ScanCoordinate(lat=51.124934, lon=17.087660),
#         ScanCoordinate(lat=51.124671, lon=17.089130),
#         ScanCoordinate(lat=51.124247, lon=17.089023),
#         ScanCoordinate(lat=51.124389, lon=17.087199),
#     ]
#
#     detections_queue: DetectionsQueue = Queue()
#     drone_telem_data = DroneTelemData(0, 0, 0, "")
#     robur_mock = RoburMock(detections_queue, drone_telem_data, scan_polygon)
#
#     assert len(robur_mock.shapes_list) == 20


def test_generate_sad_list():
    scan_polygon = [
        ScanCoordinate(lat=51.124934, lon=17.087660),
        ScanCoordinate(lat=51.124671, lon=17.089130),
        ScanCoordinate(lat=51.124247, lon=17.089023),
        ScanCoordinate(lat=51.124389, lon=17.087199),
    ]

    detections_queue: DetectionsQueue = Queue()
    drone_telem_data = DroneTelemData(0, 0, 0, "")
    robur_mock = RoburMock(detections_queue, drone_telem_data, scan_polygon)

    circles = 0
    real_figure = 0

    for shape in robur_mock.sad_list:
        if shape.class_name == "circle":
            circles += 1

        if not shape.fake_figure:
            real_figure += 1

    assert len(robur_mock.sad_list) == 100
    assert circles == 10
    assert real_figure == 10


def test_try_to_detect_shapes():

    return

    scan_polygon = [
        ScanCoordinate(lat=51.124934, lon=17.087660),
        ScanCoordinate(lat=51.124671, lon=17.089130),
        ScanCoordinate(lat=51.124247, lon=17.089023),
        ScanCoordinate(lat=51.124389, lon=17.087199),
    ]

    shapes_position = [
        Shape(
            lat=51.12481828084262,
            lon=17.087625093222986,
            class_name="circle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12446654279231,
            lon=17.087823309992302,
            class_name="triangle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12430368095388,
            lon=17.088359083037624,
            class_name="circle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12453440909,
            lon=17.089036721340314,
            class_name="square",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12439552402313,
            lon=17.0874008967258,
            class_name="triangle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.124313486667496,
            lon=17.08869401559578,
            class_name="square",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12456312723994,
            lon=17.088333337450457,
            class_name="triangle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12447502115695,
            lon=17.08732677178818,
            class_name="circle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12474601998646,
            lon=17.087748438078783,
            class_name="circle",
            color="",
            fake_figure=False,
        ),
        Shape(
            lat=51.12430025322368,
            lon=17.088805321095936,
            class_name="triangle",
            color="",
            fake_figure=False,
        ),
    ]

    detections_queue: DetectionsQueue = Queue()
    drone_telem_data = DroneTelemData(51.12429019627337, 17.088584303855896, 40, "")
    robur_mock = RoburMock(detections_queue, drone_telem_data, scan_polygon)
    robur_mock.shapes_list = shapes_position

    robur_mock.try_to_detect_shapes(drone_telem_data, robur_mock.shapes_list)

    while detections_queue.qsize() != 3:
        time.sleep(1)

    detected_shape = detections_queue.get(timeout=1)
    assert (
        distance_between_gps_points(
            detected_shape,
            DroneTelemData(
                lat=51.12430368095388,
                lon=17.088359083037624,
                alt=0,
                finished_command_timestamp="",
            ),
        )
        < 4
    )
    assert detected_shape.className == "circle"

    detected_shape = detections_queue.get(timeout=1)
    assert (
        distance_between_gps_points(
            detected_shape,
            DroneTelemData(
                lat=51.124313486667496,
                lon=17.08869401559578,
                alt=0,
                finished_command_timestamp="",
            ),
        )
        < 4
    )
    assert detected_shape.className == "square"

    detected_shape = detections_queue.get(timeout=1)
    assert (
        distance_between_gps_points(
            detected_shape,
            DroneTelemData(
                lat=51.12430025322368,
                lon=17.088805321095936,
                alt=0,
                finished_command_timestamp="",
            ),
        )
        < 4
    )
    assert detected_shape.className == "triangle"
