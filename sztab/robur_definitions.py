import json
from dataclasses import dataclass
from enum import Enum
from typing import BinaryIO, Dict, List, Union

from fastapi import UploadFile

FOCAL_LENGTH = 20.0
SENSORW = 23.52941
SENSORH = 15.68627

# [23.52941, 15.68627]
@dataclass
class Detection:
    x: int
    y: int
    w: int
    h: int
    classId: int
    className: str  # Possible values:
    confidence: float
    area: List[float]
    pos_x: float
    pos_y: float
    lat: float
    lon: float
    color_rgb: List[float]
    color: str
    fake_figure: bool
    filename: str
    altitude: float  # Alt the drone was in when it send the picture


def log_detection(detection: Detection):
    if detection.fake_figure:
        class_name = "fake"
    else:
        class_name = detection.className

    print(json.dumps({f"detection_{class_name}": [detection.lon, detection.lat]}))


@dataclass
class PhotoForRobur:
    frame: Union[UploadFile, BinaryIO]
    lat: float
    lon: float
    alt: float
    heading: float
    pitch: float
    yaw: float
    roll: float

    def get_photo_metadata(self) -> Dict:
        metadata_dict = {
            "lat": self.lat,
            "lon": self.lon,
            "alt": self.alt,
            "heading": self.heading,
            "pitch": self.pitch,
            "yaw": self.yaw,
            "roll": self.roll,
            "focal_length": FOCAL_LENGTH,
            "sensorw": SENSORW,
            "sensorh": SENSORH,
        }
        return metadata_dict
