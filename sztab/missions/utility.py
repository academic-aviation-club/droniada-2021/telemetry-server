import logging
import time

from sztab.api import DroneTelemData
from sztab.api.models import CommandForDrone
from sztab.robur import Detection

logger = logging.getLogger(__name__)


def wait_takeoff(drone_data: DroneTelemData):
    while drone_data.alt < 8:
        logger.info("Waiting for the vehicle to takeoff")
        time.sleep(2)


def command_finished(
    command: CommandForDrone, drone_telem_data: DroneTelemData
) -> bool:
    return str(command.timestamp) == str(drone_telem_data.finished_command_timestamp)
