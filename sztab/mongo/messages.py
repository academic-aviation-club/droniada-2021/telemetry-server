import datetime


def gps_rpy(
    vehicle_type: str,
    vehicle_id: int,
    gps_latitude: float,
    gps_longitude: float,
    gps_altitude: float,
    roll: float,
    pitch: float,
    yaw: float,
):
    """
    This function is used to return completed dictionary with location (GPS) and orientation (if its possible).

    :param vehicle_type: [string] select the type of vehicle,
        Choose these values: ["ugv", "uav"]
        ugv for driving vehicles,
        uav for drones or other flying vehicles
    :param vehicle_id: [int]
    :param gps_latitude: [float] Geographic coordinate of the center of the drone (latitude)
        Latitude [degrees]. Positive is north of equator; negative is south.
    :param gps_longitude: [float] Geographic coordinate of the center of the drone (longitude)
        Longitude [degrees]. Positive is east of prime meridian; negative is west.
    :param gps_altitude: [float] AGL - Above ground level -  flight altitude to terrain (altitude)
    :param roll: [float] Angle defining drone's orientation - roll (angle in radians).
    :param pitch: [float] Angle defining drone's orientation - pitch (angle in radians).
    :param yaw: [float] Angle defining drone's orientation - yaw (angle in radians).
    :return: [dict] completed dictionary
    """
    return dict(
        vehicle_type=vehicle_type,
        vehicle_id=vehicle_id,
        gps_latitude=float(gps_latitude),
        gps_longitude=float(gps_longitude),
        gps_altitude=float(gps_altitude),
        roll=float(roll),
        pitch=float(pitch),
        yaw=float(yaw),
    )


def col_shape(
    shape: str,
    color: str,
    surface: float,
    gps_latitude: float,
    gps_longitude: float,
    img: bytes,
):
    """
    This function is used to return completed dictionary with data from col_shape.
    :param shape: [string] Shape of figure.
        Choose these values: ["circle", "square", "triangle"]
    :param color: [string] Color of figure.
        Choose these values: ["bronze", "beige", "gold"]
    :param surface: [float] Surface of figure, must be greater than or equal to 0
    :param gps_latitude: [float] Geographic coordinate of the center of the area (latitude)
        Latitude [degrees]. Positive is north of equator; negative is south.
    :param gps_longitude: [float] Geographic coordinate of the center of the area (longitude)
        Longitude [degrees]. Positive is east of prime meridian; negative is west.
    :param img: [bytes] Photography
    :return: [dict] completed dictionary
    """
    return dict(
        shape=shape,
        color=color,
        surface=float(surface),
        gps_latitude=float(gps_latitude),
        gps_longitude=float(gps_longitude),
        picture=img,
    )


def col_sosnowsky(
    circle_surface: float,
    square_surface: float,
    triangle_surface: float,
    gps_latitude: float,
    gps_longitude: float,
    img: bytes,
):
    """
    This function is used to return completed dictionary with data from col_sosnowsky.
    :param triangle_surface: [float] Surface of figure, must be greater than or equal to 0
    :param square_surface: [float] Surface of figure, must be greater than or equal to 0
    :param circle_surface: [float] Surface of figure, must be greater than or equal to 0
    :param gps_latitude: [float] Geographic coordinate of the center of the area (latitude)
        Latitude [degrees]. Positive is north of equator; negative is south.
    :param gps_longitude: [float] Geographic coordinate of the center of the area (longitude)
        Longitude [degrees]. Positive is east of prime meridian; negative is west.
    :param img: [bytes] Photography
    :return: [dict] completed dictionary
    """
    return dict(
        circle_surface=float(circle_surface),
        square_surface=float(square_surface),
        triangle_surface=float(triangle_surface),
        gps_latitude=float(gps_latitude),
        gps_longitude=float(gps_longitude),
        picture=img,
    )


def tol_map(img: bytes):
    """
    This function is used to return completed dictionary with data from tol_map.
    :param img: [bytes] Photography
    :return: [dict] completed dictionary
    """
    return dict(picture=img)


def tol_detect(
    pathogen_type: str, gps_latitude: float, gps_longitude: float, img: bytes
):
    """
    This function is used to return completed dictionary with data from tol_detect.
    :param pathogen_type: [string] Type of pathogen.
        Choose these values: ["bronze", "beige", "gold"]
    :param gps_latitude: [float] Geographic coordinate of the center of the area (latitude)
        Latitude [degrees]. Positive is north of equator; negative is south.
    :param gps_longitude: [float] Geographic coordinate of the center of the area (longitude)
        Longitude [degrees]. Positive is east of prime meridian; negative is west.
    :param img: [bytes] Photography
    :return: [dict] completed dictionary
    """
    return dict(
        pathogen_type=pathogen_type,
        gps_latitude=float(gps_latitude),
        gps_longitude=float(gps_longitude),
        picture=img,
    )


def tol_apply(pesticides_type: str, gps_latitude: float, gps_longitude: float):
    """
    This function is used to return completed dictionary with data from tol_apply.
    :param pesticides_type: [string] Type of spray application used.
        Choose these values: ["yellow", "orange"]
    :param gps_latitude: [float] Geographic coordinate of the center of the area (latitude)
        Latitude [degrees]. Positive is north of equator; negative is south.
    :param gps_longitude: [float] Geographic coordinate of the center of the area (longitude)
        Longitude [degrees]. Positive is east of prime meridian; negative is west.
    :return: [dict] completed dictionary
    """
    return dict(
        pesticides_type=pesticides_type,
        gps_latitude=float(gps_latitude),
        gps_longitude=float(gps_longitude),
    )


def start():
    """
    This function is used to return completed dictionary with time of signalling the start of the mission.
    :return: [dict] completed dictionary
    """
    return dict(date=datetime.datetime.now())


def finish():
    """
    This function is used to return completed dictionary with time of signalling the end of the mission.
    :return: [dict] completed dictionary
    """
    return dict(date=datetime.datetime.now())
