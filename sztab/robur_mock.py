import json
import logging
import math
import queue
import random
import threading
import time
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from queue import Queue
from typing import List

import numpy as np
from geopy import distance
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

from sztab.robur_definitions import Detection, log_detection

from .api import DroneTelemData
from .api.models import ScanCoordinate
from .robur import DetectionsQueue

logger = logging.getLogger(__name__)


@dataclass
class Shape:
    lat: float
    lon: float
    class_name: str
    color: str
    fake_figure: bool


class ClassName(Enum):
    CIRCLE = "circle"
    SQUARE = "square"
    TRIANGLE = "triangle"


class Color(Enum):
    PARCH = "parch"
    MACZNIAK = "mączniak jabłoni"


FIELD_OF_VIEW_QUOTIENT = 0.50 * 1.42


def get_mock_detection(
    class_name: str, color: str, is_fake: bool, lat: float, lon: float, alt: float
) -> Detection:
    return Detection(
        x=0,
        y=0,
        w=0,
        h=0,
        classId=1,
        className=class_name,
        confidence=100,
        area=[1.01, 1.02, 1.03],
        pos_x=0,
        pos_y=0,
        lat=lat,
        lon=lon,
        color_rgb=[],
        color=color,
        fake_figure=is_fake,
        filename="mgoQiKazXhPeLQ",
        altitude=alt,
    )


class RoburMockThread(ABC):
    def __init__(self):
        self.task_end = threading.Event()
        self.task_thread = threading.Thread(
            name=self.__class__.__name__,
            target=self._run,
        )

    def run(self):
        self.task_thread.start()

    def stop(self):
        logger.info("Stopping robur mock %s", self.__class__.__name__)
        self.task_end.set()
        self.task_thread.join()

    @abstractmethod
    def _run(self):
        """Runs the thread"""


class RoburMock(RoburMockThread):
    def __init__(
        self,
        detections_queue: DetectionsQueue,
        drone_telem_data: DroneTelemData,
        polygon: List[ScanCoordinate],
        seconds_between_images=10,
        sad: bool = False,
    ):
        super().__init__()
        self.detections_queue = detections_queue
        self.drone_telem_data = drone_telem_data
        self.seconds_between_images = seconds_between_images
        self.polygon: List[ScanCoordinate] = polygon
        self.sad = sad
        self.sad_list = self.generate_sad_list()
        # self.shapes_list = self.generate_shapes_list()
        self.shapes_list = self.generate_shapes_list_new_sad_quick_hack_xd()
        self.log_mock_points()

    def log_mock_points(self):
        if self.sad:
            for point in self.sad_list:
                print(json.dumps({"detection_mock": [point.lon, point.lat]}))
        else:
            for point in self.shapes_list:
                print(json.dumps({"detection_mock": [point.lon, point.lat]}))

    def _run(self):
        logger.info("Robur mock thread started")
        while True:
            if self.task_end.is_set():
                return

            time.sleep(self.seconds_between_images)

            if self.drone_telem_data.alt < 4:
                logger.info("Drone below 4 meters, not taking any pictures")
                continue
            else:
                telem: DroneTelemData = self.drone_telem_data
                if not self.sad:
                    self.try_to_detect_shapes(telem=telem, list=self.shapes_list)
                else:
                    self.try_to_detect_shapes(telem=telem, list=self.sad_list)

    def generate_sad_list(self) -> List[Shape]:
        """Generuje sad"""

        reference_point = ScanCoordinate(lat=52.239716, lon=16.230710)
        sad_points: List[Shape] = []
        north_shift = 0
        east_shift = 0

        # generating healthy trees
        for _ in range(5):
            for _ in range(20):
                east_shift += 5
                tree_position = RoburMock.shift_of_coordinate(
                    reference_point, north_shift, east_shift
                )
                tree = Shape(tree_position.lat, tree_position.lon, "square", "", True)
                sad_points.append(tree)
            north_shift -= 5
            east_shift = 0

        infected_trees_pos: List[int] = []
        # generating pos of infected trees
        while len(infected_trees_pos) != 10:
            random_tree = random.randint(0, 99)
            try:
                infected_trees_pos.index(random_tree)
            except:
                infected_trees_pos.append(random_tree)

        for tree_pos in infected_trees_pos:
            color = random.choice(list(Color))
            sad_points[tree_pos].class_name = "circle"
            sad_points[tree_pos].color = color.value
            sad_points[tree_pos].fake_figure = False

        return sad_points

    def generate_shapes_list(self) -> List[Shape]:
        logger.info("Start generating shapes list")
        max_lat = max(self.polygon, key=lambda p: p.lat).lat
        max_lon = max(self.polygon, key=lambda p: p.lon).lon
        min_lat = min(self.polygon, key=lambda p: p.lat).lat
        min_lon = min(self.polygon, key=lambda p: p.lon).lon

        objects_list: List[Shape] = []

        while len(objects_list) != 1:
            logger.info("Generating sosna object")
            random_lat = random.uniform(min_lat, max_lat)
            random_lon = random.uniform(min_lon, max_lon)

            generated_object = Shape(random_lat, random_lon, "sosna", "", False)
            if self.is_in_geofence(generated_object, self.polygon):
                logger.info("Sosna object generated")
                objects_list.append(generated_object)

        while len(objects_list) != 10:
            logger.info("Generating normal object")
            random_lat = random.uniform(min_lat, max_lat)
            random_lon = random.uniform(min_lon, max_lon)
            class_name = random.choice(list(ClassName))

            generated_object = Shape(
                random_lat, random_lon, class_name.value, "", False
            )
            if self.is_in_geofence(generated_object, self.polygon):
                logger.info("Normal object generated")
                objects_list.append(generated_object)

        while len(objects_list) != 20:
            logger.info("Generating fake object")
            random_lat = random.uniform(min_lat, max_lat)
            random_lon = random.uniform(min_lon, max_lon)
            class_name = random.choice(list(ClassName))

            generated_object = Shape(random_lat, random_lon, class_name.value, "", True)
            if self.is_in_geofence(generated_object, self.polygon):
                logger.info("Fake object generated")
                objects_list.append(generated_object)

        return objects_list

    def generate_shapes_list_new_sad_quick_hack_xd(self) -> List[Shape]:
        logger.info("Start generating shapes list")
        max_lat = max(self.polygon, key=lambda p: p.lat).lat
        max_lon = max(self.polygon, key=lambda p: p.lon).lon
        min_lat = min(self.polygon, key=lambda p: p.lat).lat
        min_lon = min(self.polygon, key=lambda p: p.lon).lon

        objects_list: List[Shape] = []

        while len(objects_list) != 5:
            logger.info("Generating normal object")
            random_lat = random.uniform(min_lat, max_lat)
            random_lon = random.uniform(min_lon, max_lon)

            generated_object = Shape(
                random_lat, random_lon, "circle", "Mączniak prawdziwy", False
            )
            if self.is_in_geofence(generated_object, self.polygon):
                logger.info("Normal object generated")
                objects_list.append(generated_object)

        while len(objects_list) != 10:
            logger.info("Generating normal object")
            random_lat = random.uniform(min_lat, max_lat)
            random_lon = random.uniform(min_lon, max_lon)

            generated_object = Shape(
                random_lat, random_lon, "circle", "Mączniak rzekomy", False
            )
            if self.is_in_geofence(generated_object, self.polygon):
                logger.info("Normal object generated")
                objects_list.append(generated_object)

        return objects_list

    def is_in_geofence(
        self, generated_object: Shape, polygon_list: List[ScanCoordinate]
    ) -> bool:
        temp_points = list()
        for temp_point in polygon_list:
            temp_points.append((temp_point.lat, temp_point.lon))

        lons_lats_vect = np.asarray(temp_points)
        polygon = Polygon(lons_lats_vect)
        point = Point(generated_object.lat, generated_object.lon)
        return bool(polygon.contains(point))

    def try_to_detect_shapes(self, telem: DroneTelemData, list: List[Shape]) -> None:
        logger.info("Detecting shapes")
        for shape in list:
            if self.is_in_drones_view(telem=telem, generated_object=shape):
                # HARDCODING
                logger.info("Shape detected")

                rand = random.randint(0, 100)
                if rand + 30 < telem.alt:
                    logger.info("Chuj ci w dupę")
                    return

                rand_shift_n = random.uniform(
                    -2.18 * telem.alt / 50, 2.18 * telem.alt / 50
                )
                rand_shift_e = random.uniform(
                    -2.18 * telem.alt / 50, 2.18 * telem.alt / 50
                )
                pos_noise = self.shift_of_coordinate(
                    ScanCoordinate(lat=shape.lat, lon=shape.lon),
                    rand_shift_n,
                    rand_shift_e,
                )

                detection = get_mock_detection(
                    shape.class_name,
                    shape.color,
                    shape.fake_figure,
                    pos_noise.lat,
                    pos_noise.lon,
                    telem.alt,
                )
                log_detection(detection)
                self.detections_queue.put(detection)

    def is_in_drones_view(self, telem: DroneTelemData, generated_object: Shape) -> bool:
        distance_to_shape = RoburMock.distance_between_gps_points(
            p1=telem, p2=generated_object
        )

        if distance_to_shape < telem.alt * FIELD_OF_VIEW_QUOTIENT:
            return True
        return False

    @staticmethod
    def distance_between_gps_points(p1: DroneTelemData, p2: Shape) -> float:
        """
        distance in meters
        """
        return float(distance.distance([p1.lat, p1.lon], [p2.lat, p2.lon]).meters)

    @staticmethod
    def north_to_lat(n: float, lat: float) -> float:
        """
        Converts north shift to a latitude
        """
        return (n / (40075704.0 / 360)) + lat

    @staticmethod
    def east_to_lon(e: float, lat: float, lon: float) -> float:
        """
        Converts east shift to latitude and longitude
        """
        lat = math.radians(lat)
        return (e / (math.cos(lat) * (40075704.0 / 360.0))) + lon

    @staticmethod
    def shift_of_coordinate(
        point: ScanCoordinate, north_shift: float, east_shift: float
    ) -> ScanCoordinate:
        """
        Calcs the coordinates of a shifted point
        """
        lat = RoburMock.north_to_lat(north_shift, point.lat)
        lon = RoburMock.east_to_lon(east_shift, point.lat, point.lon)
        geofence_point = ScanCoordinate(lat=lat, lon=lon)
        return geofence_point


if __name__ == "__main__":
    robur_mock_geo_points = [
        ScanCoordinate(lat=51.124719163265745, lon=17.0871748052045),
        ScanCoordinate(lat=51.124357224947275, lon=17.08711847881519),
        ScanCoordinate(lat=51.1243403905379, lon=17.08824768881046),
        ScanCoordinate(lat=51.12469054499051, lon=17.088185998003116),
    ]
    drone_telem_data = DroneTelemData(0, 0, 0, "")
    detections_queue: DetectionsQueue = Queue()
    robur = RoburMock(detections_queue, drone_telem_data, robur_mock_geo_points)
    robur.stop()
