import time
from datetime import datetime
from enum import Enum
from typing import List, Optional

from pydantic import BaseModel, Field, PrivateAttr  # pylint: disable=no-name-in-module

from sztab.mongo.mongo_definitions import PesticideType


class CommandType(Enum):
    IDLE = "IDLE"
    TAKEOFF = "TAKEOFF"
    SCAN = "SCAN"
    DROP_PESTICIDES = "DROP_PESTICIDES"
    RTL = "RTL"
    FLY_PATH = "FLY_PATH"


class ScanCoordinate(BaseModel):
    lat: float
    lon: float


class PointToFlyTo(BaseModel):
    lat: float
    lon: float
    alt: float


class ScanArguments(BaseModel):
    polygon: list[ScanCoordinate]
    altitude: float


def current_timestamp():
    return int(time.time())


class CommandForDrone(BaseModel):
    command_type: CommandType = CommandType.IDLE
    scan_arguments: Optional[ScanArguments]
    points_to_fly_to: Optional[List[PointToFlyTo]]
    pesticide_type: Optional[PesticideType]
    timestamp: int = Field(default_factory=current_timestamp)


class Attitude(BaseModel):
    pitch: float
    yaw: float
    roll: float
