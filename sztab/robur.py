import asyncio
import json
import logging
import threading
import time
import traceback
from dataclasses import dataclass
from queue import Queue
from typing import BinaryIO, Coroutine, List

import dacite
import requests
from requests.exceptions import HTTPError
from requests.models import ReadTimeoutError
from starlette.datastructures import UploadFile

from sztab.robur_definitions import Detection, PhotoForRobur, log_detection

URL = "http://79.189.78.141:9999/predictDroniada"
DEFAULT_ROBUR_TIMEOUT = 10

logger = logging.getLogger(__name__)


DetectionsQueue = Queue[Detection]

RoburQueue = Queue[PhotoForRobur]


class Robur:
    def __init__(
        self, image_queue: RoburQueue, detections_queue: DetectionsQueue
    ) -> None:
        self.url = URL
        self.robur_thread = threading.Thread(target=self.robur_thread_function)
        self.image_queue = image_queue
        self.detections_queue = detections_queue

    def run(self):
        self.robur_thread.start()

    def robur_thread_function(self):
        logger.critical("Robur thread started")
        while True:
            time.sleep(1)
            image_to_process = self.image_queue.get()
            self.process_image_until_success(image_to_process)

    def process_image_until_success(self, image: PhotoForRobur):

        response = self.ask_robur(image)

        if response is not None:
            logger.info("Got good response from robur")
            self.process_robur_response(response.text, image.alt)
        else:
            logger.error(f"Did not get proper response from robur")

    def ask_robur(self, image: PhotoForRobur, timeout=10, max_retries_num=5):
        frame_to_read = image.frame

        if isinstance(frame_to_read, BinaryIO):
            frame = frame_to_read.read()
        elif isinstance(frame_to_read, UploadFile):
            frame = asyncio.run(frame_to_read.read())  # type: ignore
        else:
            logger.error(
                "Invalid type passed to ask_robur: %s", str(type(frame_to_read))
            )
            return None

        files = {"image": frame}
        data = image.get_photo_metadata()
        response = None
        for i in range(0, max_retries_num):
            logger.info(f"Trying to send the request for {i} time")
            try:
                logger.info("Sending post request")
                response = requests.post(
                    self.url, files=files, data=data, timeout=timeout
                )
                response.raise_for_status()
                logger.info(f"Got response from server {response.status_code}")
                return response

            except HTTPError as http_err:
                logger.error(f"Http error {http_err}")
            except requests.exceptions.Timeout:
                logger.error("Connection timed out")
            except:
                logger.error("Robur asking failed: %s", traceback.format_exc())

        return None

    def process_robur_response(self, response, altitude: float) -> None:
        logger.info("Start processing response")

        new_string = response.replace("\\", "")

        response_dict = json.loads(new_string)
        print(json.dumps(response_dict))

        filename = response_dict["filename"]

        detections = response_dict["Detections"]
        if not detections:
            logger.info(f"Nothing detected filename {filename}")
            return None

        for obj in detections:
            obj["filename"] = filename
            obj["altitude"] = altitude
            detection = dacite.from_dict(data_class=Detection, data=obj)

            log_detection(detection)
            self.detections_queue.put(detection)
            logger.info(f"Object added to queue {detection}")


if __name__ == "__main__":

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    )

    IMAGE_PATH = "/home/olga/code/telemetry-server/images/test.jpg"
    IMAGE_PATH_2 = "/home/olga/code/telemetry-server/images/test_empty.jpg"

    image_queue: Queue[PhotoForRobur] = Queue()
    photo1 = PhotoForRobur(
        frame=open(IMAGE_PATH, "rb"),
        lat=51.2,
        lon=34.5,
        alt=30,
        heading=120,
        pitch=0,
        yaw=0,
        roll=0,
    )
    photo2 = PhotoForRobur(
        frame=open(IMAGE_PATH_2, "rb"),
        lat=51.2,
        lon=34.5,
        alt=30,
        heading=120,
        pitch=0,
        yaw=0,
        roll=0,
    )
    image_queue.put(photo1)
    image_queue.put(photo2)
    # # photo1 = PhotoForRobur(frame=open(IMAGE_PATH, 'rb'), lat=51.2,
    # #  lon=34.5, alt=30, heading=120, pitch=0, yaw=0, roll=0)
    # # photo2 = PhotoForRobur(frame=open(IMAGE_PATH_2, 'rb'), lat=51.2,
    # #  lon=34.5, alt=30, heading=120, pitch=0, yaw=0, roll=0)
    image_queue.put(photo1)
    image_queue.put(photo2)

    detections_queue: Queue[Detection] = Queue()
    robur = Robur(image_queue=image_queue, detections_queue=detections_queue)
    robur.run()
