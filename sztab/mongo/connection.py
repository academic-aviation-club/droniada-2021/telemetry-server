import json
import logging
import os
import traceback
from copy import copy
from queue import Queue
from threading import Thread

import pymongo

from sztab.mongo.mongo_definitions import MongoMessage, MongoMsgType

HOST = "195.216.97.232"
PORT = 27027


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s,%(msecs)d %(levelname)s <%(threadName)s> [%(filename)s:%(lineno)d] %(message)s",
)
logger = logging.getLogger(__name__)


class MongoConnection:
    def __init__(self, user: str, password: str, host=HOST, port=PORT) -> None:
        self.client = pymongo.MongoClient(host=host, port=port)
        self.database = self.client[user]
        self.database.authenticate(user, password)
        self.messages_queue: Queue = Queue()
        self.mongo_thread = Thread(target=self._send_message_thread)

    def start_mongo_thread(self) -> None:
        logger.info("Start mongo thread")
        self.mongo_thread.start()

    def _send_message_thread(self) -> None:
        logger.info("Starting mongo thread")

        while True:
            logger.info(f"Mongo message queue size: {self.messages_queue.qsize()}")
            try:
                msg_to_send: MongoMessage = self.messages_queue.get()
                self._send_message(message=msg_to_send)
            except Exception:
                logger.error(
                    f"Eror occured in mongo message thread: {traceback.format_exc()}"
                )

    def _send_message(self, message: MongoMessage) -> None:
        logger.info("Message sending")
        try:
            msg_type = message.message_type.value

            _id = self.database[msg_type].insert_one(message.msg_content_dict)
            logger.debug(f"_id: %r", _id)
        except Exception:
            logger.error(
                f"Some error occured while sending mongo message: {traceback.format_exc()}"
            )

    def send_message(self, message: MongoMessage) -> None:
        self.messages_queue.put(message)

        # Not sure how memory management works in queues, making a copy so that the original data won't be modified
        message_dict = copy(message.msg_content_dict)

        # Don't log xxMB of image bytes
        if "picture" in message_dict:
            message_dict["picture"] = "Image bytes"

        print(
            json.dumps(
                {"mongo": {"type": message.message_type.value, "data": message_dict}},
                default=str,
            )
        )
        logger.info(f"Message of type {message.message_type.value} put in the queue")

    def clear_database(self, msg_type: str) -> None:
        col = self.database[msg_type]
        col.delete_many({})
        print(f"{col.deleted_count} documents deleted.")

    def get_records(self, msg_type) -> None:
        col = self.database[msg_type]

        for record in col.find():
            if msg_type == "col_shape":
                print(record["color"], record["shape"])
            elif msg_type == "col_sosnowsky":
                print(record["circle_surface"])
            else:
                print(record)


if __name__ == "__main__":
    mongo_connection = MongoConnection(
        os.environ["MONGO_USER"], os.environ["MONGO_PASSWORD"]
    )
    # mongo_connection.get_records(MongoMsgType.START.value)
    # mongo_connection.get_records(MongoMsgType.GPS_RPY.value)
    # mongo_connection.get_records(MongoMsgType.COL_SHAPE.value)
    # mongo_connection.get_records(MongoMsgType.COL_SOSNOWSKY.value)
    # mongo_connection.get_records(MongoMsgType.FINISH.value)
    # mongo_connection.clear_database(MongoMsgType.GPS_RPY.value)
    # mongo_connection.clear_database(MongoMsgType.START.value)
    # mongo_connection.clear_database(MongoMsgType.COL_SHAPE.value)
    # mongo_connection.clear_database(MongoMsgType.COL_SOSNOWSKY.value)
    # mongo_connection.clear_database(MongoMsgType.FINISH.value)
    # mongo_connection.get_records(MongoMsgType.GPS_RPY.value)
