from sztab.missions.detections_set import DetectionSet
from sztab.robur import Detection
from sztab.robur_mock import get_mock_detection


def test_adding_detection_of_another_class():
    detection_set = DetectionSet()

    detection_circle = get_mock_detection("circle", "whatever", False, 51, 16, 30)

    detection_square = get_mock_detection("square", "whatever", False, 51, 16, 30)

    detection_set.process_detection(detection_square)
    detection_set.process_detection(detection_circle)

    assert len(detection_set.groups) == 2


def test_adding_detection_same_class():
    detection_set = DetectionSet()

    detection_circle = get_mock_detection("circle", "whatever", False, 51, 16, 30)

    detection_square = get_mock_detection("circle", "whatever", False, 51, 16, 30)

    detection_set.process_detection(detection_square)
    detection_set.process_detection(detection_circle)

    assert len(detection_set.groups) == 1


def test_adding_detection_same_class_long_distance():
    detection_set = DetectionSet()

    detection_circle = get_mock_detection("circle", "whatever", False, 51, 16, 30)

    detection_square = get_mock_detection("circle", "whatever", False, 57, 16, 30)

    detection_set.process_detection(detection_square)
    detection_set.process_detection(detection_circle)

    assert len(detection_set.groups) == 2
