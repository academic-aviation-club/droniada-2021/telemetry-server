import json
import logging
import time
import traceback
from queue import Empty, Queue
from typing import List

import requests

from sztab.api import CommandForDrone, DroneTelemData, DroniadaApi
from sztab.api.models import CommandType, PointToFlyTo, ScanArguments, ScanCoordinate
from sztab.mongo import messages as MongoMessages
from sztab.mongo.connection import MongoConnection
from sztab.mongo.mongo_definitions import MongoMessage, MongoMsgType
from sztab.robur import DetectionsQueue, Robur, RoburQueue

from .detections_set import DetectionGroup, DetectionSet
from .utility import command_finished, wait_takeoff

logger = logging.getLogger(__name__)

NUM_OF_VALID_MARKERS = 10

# Probably not needed according to the competition rules
# NUM_OF_FAKE_MARKERS = 10

THREE_COLORS_FLIGHT_ALTITUDE = 50

# TODO: finish
_ROBUR_SHAPE_TO_MONGO_COLOR = {
    "triangle": "bronze",
    "circle": "gold",
    "beka": "beige",
}

_ROBUR_TO_MONGO_COLOR = {
    "Frytoftoroza": "bronze",
    "Mączniak rzekomy": "gold",
    "Mączniak prawdziwy": "beige",
}


def run_three_colors(
    scan_path: List[PointToFlyTo],
    drone_telem_data: DroneTelemData,
    detections_queue: DetectionsQueue,
    api: DroniadaApi,
):
    wait_takeoff(drone_telem_data)

    logger.info("Scanning!")
    scan_command = CommandForDrone(
        command_type=CommandType.FLY_PATH,
        points_to_fly_to=scan_path,
    )

    api.current_command_for_drone = scan_command
    detection_set = DetectionSet()

    while True:

        if command_finished(scan_command, drone_telem_data):
            logger.critical("Scan finished, returning to launch")
            break

        if drone_telem_data.alt < 3:
            logger.critical("Altitude below 3, marking the mission as finished")
            break

        try:
            detection = detections_queue.get(timeout=5)
        except Empty:
            continue

        current_detection_set_len = len(detection_set.groups)
        detection_set.process_detection(detection)

        if len(detection_set.groups) != current_detection_set_len:
            logger.critical("Reporting 3 colors detection")
            report_3colors_detection(detection_set.groups[-1], api.mongo_connection)

        # if detected_all_figures(detection_set):
        #     logger.critical("Detected all figures, returning to launch")
        #     break

    logger.info("RTL")
    api.current_command_for_drone = CommandForDrone(command_type=CommandType.RTL)

    while drone_telem_data.alt > 2:
        logger.info("Waiting for drone to land")
        time.sleep(1)

    # Just to be sure
    time.sleep(5)

    # SEND MISSION END
    report_mission_end(connection=api.mongo_connection)

    while True:
        logger.info("Mission finished")
        time.sleep(5)


def report_3colors_detection(detection: DetectionGroup, connection: MongoConnection):
    if detection.fake_figure:
        return

    if detection.detection_class == "sosna":
        detection_object = detection.detections[0]
        lat, lon = detection.average_position

        circle_area = detection_object.area[0]
        square_area = detection_object.area[1]
        triangle_area = detection_object.area[2]

        try:
            response = requests.get(
                f"http://robur.agro.pl:9999/getImage?filename={detection_object.filename}",
                timeout=20,
            )

            message = MongoMessage(
                message_type=MongoMsgType.COL_SOSNOWSKY,
                msg_content_dict=MongoMessages.col_sosnowsky(
                    circle_surface=circle_area,
                    square_surface=square_area,
                    triangle_surface=triangle_area,
                    gps_latitude=lat,
                    gps_longitude=lon,
                    img=response.content,
                ),
            )

            connection.send_message(message=message)
        except Exception as ex:
            logger.exception(ex)

    else:
        detection_object = detection.detections[0]

        try:
            response = requests.get(
                f"http://robur.agro.pl:9999/getImage?filename={detection_object.filename}",
                timeout=20,
            )

            area = detection_object.area[0]
            detection_color = detection_object.color

            lat, lon = detection.average_position

            message = MongoMessage(
                message_type=MongoMsgType.COL_SHAPE,
                msg_content_dict=MongoMessages.col_shape(
                    shape=detection.detection_class,
                    color=_ROBUR_TO_MONGO_COLOR.get(detection_color, "gold"),
                    gps_latitude=lat,
                    gps_longitude=lon,
                    surface=area,
                    img=response.content,
                ),
            )

            connection.send_message(message)

        except Exception as e:
            logger.exception(e)


def report_mission_end(connection: MongoConnection):
    logger.critical("Finishing mission")
    connection.send_message(
        MongoMessage(
            message_type=MongoMsgType.FINISH, msg_content_dict=MongoMessages.finish()
        )
    )


def detected_all_figures(detection_set: DetectionSet) -> bool:
    valid_detections = filter(
        lambda detection_group: detection_group.fake_figure is False,
        detection_set.groups,
    )

    fake_detections = filter(
        lambda detection_group: detection_group.fake_figure is True,
        detection_set.groups,
    )

    num_of_valid_detections = len(list(valid_detections))
    num_of_fake_detections = len(list(fake_detections))

    print(
        json.dumps(
            {
                "total_valid_detections": num_of_valid_detections,
                "total_fake_detections": num_of_fake_detections,
            }
        )
    )

    if num_of_valid_detections >= NUM_OF_VALID_MARKERS:
        return True

    return False
