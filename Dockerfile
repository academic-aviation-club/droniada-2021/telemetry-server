FROM registry.gitlab.com/academic-aviation-club/droniada-2021/droniada-docker-builder:latest

RUN mkdir /app
COPY poetry.lock /app
COPY poetry.toml /app
COPY pyproject.toml /app

WORKDIR /app
RUN poetry install --no-dev

COPY . /app


CMD [".venv/bin/python", "-u", "-m", "sztab"]

