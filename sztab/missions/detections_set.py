import logging
from dataclasses import asdict, dataclass
from typing import List

from geopy.distance import distance

from sztab.robur import Detection

logger = logging.getLogger(__name__)

Lat = float
Lon = float


@dataclass
class DetectionGroup:
    GROUP_MATCHING_METERS_EPSILON = 3

    detection_class: str
    fake_figure: bool
    detections: List[Detection]

    @property
    def calculate_area(self):  # TODO: add predicates, limits, sanity checks
        return sum(sum(detection.area) for detection in self.detections) / len(
            self.detections
        )

    @property
    def average_position(self) -> tuple[Lat, Lon]:
        lat = sum(detection.lat for detection in self.detections) / len(self.detections)
        lon = sum(detection.lon for detection in self.detections) / len(self.detections)

        return lat, lon

    # TODO: remaining calculation functions

    def is_part_of(self, detection: Detection) -> bool:
        if detection.className != self.detection_class:
            return False

        lat, lon = self.average_position
        if (
            distance([lat, lon], [detection.lat, detection.lon]).meters
            < self.GROUP_MATCHING_METERS_EPSILON
        ):
            return True

        return False

    def add_detection(self, detection: Detection):
        self.detections.append(detection)


class DetectionSet:
    def __init__(self):
        self.groups: List[DetectionGroup] = list()

    def process_detection(self, detection: Detection):
        printable_detection = str(asdict(detection))

        for group in self.groups:
            if group.is_part_of(detection):
                logger.info(
                    "Detection %s classified as part of a group", printable_detection
                )
                group.add_detection(detection)
                return

        logger.info("Creating a new detection group for %s", printable_detection)
        self.groups.append(
            DetectionGroup(
                detection_class=detection.className,
                detections=[detection],
                fake_figure=detection.fake_figure,
            )
        )
