from queue import Queue

import pytest

from sztab.robur import Robur


@pytest.fixture
def sample_response_from_robur():
    return """
    {
  "filename": "dPIB1TlArfFMUg",
  "Detections": [
    {
      "x": 2913,
      "y": 1976,
      "w": 182,
      "h": 200,
      "classId": 0,
      "className": "square",
      "confidence": 0.9960401654243469,
      "area": [
        0.36516494434870705
      ],
      "pos_x": 2.996258627880443,
      "pos_y": -4.07398357534168,
      "lat": 51.229963361777585,
      "lon": 17.343043031246076,
      "color_rgb": [
        22,
        164,
        152
      ],
      "color": "Frytoftoroza",
      "fake_figure": false
    },
    {
      "x": 4088,
      "y": 2042,
      "w": 149,
      "h": 168,
      "classId": 0,
      "className": "square",
      "confidence": 0.9455715417861938,
      "area": [
        0.32031373916201467
      ],
      "pos_x": 7.119367806294931,
      "pos_y": -4.371858547273572,
      "lat": 51.22996068288696,
      "lon": 17.343102245930073,
      "color_rgb": [
        20,
        168,
        161
      ],
      "color": "Frytoftoroza",
      "fake_figure": false
    }
  ]
}
"""


def test_robur_response_parsing(sample_response_from_robur):

    input_queue = Queue()
    output_queue = Queue()

    robur = Robur(input_queue, output_queue)
    robur.process_robur_response(sample_response_from_robur, altitude=50)
