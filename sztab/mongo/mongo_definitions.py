from dataclasses import dataclass
from enum import Enum
from typing import Dict


class MongoMsgType(Enum):
    GPS_RPY = "gps_rpy"
    COL_SHAPE = "col_shape"
    COL_SOSNOWSKY = "col_sosnowsky"
    TOL_MAP = "tol_map"
    TOL_DETECT = "tol_detect"
    TOL_APPLY = "tol_apply"
    START = "start"
    FINISH = "finish"


@dataclass
class MongoMessage:
    message_type: MongoMsgType
    msg_content_dict: Dict


class PesticideType(Enum):
    YELLOW = "yellow"
    ORANGE = "orange"
