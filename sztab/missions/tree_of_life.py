import json
import logging
import queue
import time
from typing import List

import requests
from requests.api import get

from sztab.api import CommandForDrone, DroneTelemData, DroniadaApi
from sztab.api.models import CommandType, PointToFlyTo, ScanCoordinate
from sztab.mongo import connection
from sztab.mongo import messages as MongoMessages
from sztab.mongo.connection import MongoConnection
from sztab.mongo.mongo_definitions import MongoMessage, MongoMsgType, PesticideType
from sztab.robur import Detection, DetectionsQueue, Robur, RoburQueue

from .detections_set import DetectionGroup, DetectionSet
from .utility import command_finished, wait_takeoff

logger = logging.getLogger(__name__)

NUM_OF_INFECTED_TREES = 10
MINIMUM_NUMBER_OF_TREES_TO_DETECT = 8
DROP_ALTITUDE = 7

_ROBUR_TO_MONGO_COLOR = {
    "Frytoftoroza": "bronze",
    "Mączniak rzekomy": "gold",
    "Mączniak prawdziwy": "beige",
}


_ROBUR_TO_PESTICIDE = {
    "Mączniak rzekomy": PesticideType.YELLOW,
    "Mączniak prawdziwy": PesticideType.ORANGE,
}

# PARCH ZLOTE KOŁA zół
# MACZNIAK -> Bezowe kola pomaranczowe
# Brazowe koła -> zdrowe drzewa


def run_tree_of_life(
    low_alt_scan_path: List[PointToFlyTo],
    drone_telem_data: DroneTelemData,
    detections_queue: DetectionsQueue,
    api: DroniadaApi,
):
    wait_takeoff(drone_telem_data)

    # Phase 1: find infections

    detection_set = DetectionSet()

    run_scan(
        detection_set,
        api,
        detections_queue,
        drone_telem_data,
        low_alt_scan_path,
    )

    # Phase 2: nuke the fucker

    infected_trees = get_infected_trees(detection_set)
    logger.critical("Applying pesticides to %i trees", len(infected_trees))

    infected_trees.sort(key=lambda tree: tree.average_position[1])

    for tree in infected_trees:
        lat, lon = tree.average_position
        drop_pesticides(
            api,
            PointToFlyTo(lat=lat, lon=lon, alt=DROP_ALTITUDE),
            tree.detections[0].color,
            drone_telem_data,
        )

    api.current_command_for_drone = CommandForDrone(command_type=CommandType.RTL)

    while drone_telem_data.alt > 4:
        logger.critical("Waiting for vehicle to land")
        time.sleep(2)

    report_mission_end(api.mongo_connection)

    while True:
        logger.critical("Mission finished")
        time.sleep(5)


def get_infected_trees(detection_set: DetectionSet):
    infected_trees = list(
        filter(
            lambda detection: detection.fake_figure is False
            and detection.detection_class == "circle",
            detection_set.groups,
        )
    )

    print(json.dumps({"total_valid_detections": len(infected_trees)}))

    return infected_trees


def run_scan(
    detection_set: DetectionSet,
    api: DroniadaApi,
    detections_queue: DetectionsQueue,
    drone_telem_data: DroneTelemData,
    scan_path: List[PointToFlyTo],
):
    # Tis fly path should not take more than 3-5 minutes
    high_alt_scan_command = CommandForDrone(
        command_type=CommandType.FLY_PATH,
        points_to_fly_to=scan_path,
    )
    api.current_command_for_drone = high_alt_scan_command
    while True:

        if command_finished(high_alt_scan_command, drone_telem_data):
            break
        try:
            detection: Detection = detections_queue.get(timeout=10)
            current_infected_trees_num = len(
                get_infected_trees(detection_set=detection_set)
            )
            detection_set.process_detection(detection)

            infected_trees = get_infected_trees(detection_set=detection_set)
            if len(infected_trees) != current_infected_trees_num:
                logger.critical("Reporting 3 colors detection")
                report_infected_tree(
                    infected_tree=infected_trees[-1], connection=api.mongo_connection
                )

            if len(infected_trees) >= NUM_OF_INFECTED_TREES:
                return

        except queue.Empty:
            # This will reset the drone command and
            # force it to take another picture
            continue


def drop_pesticides(
    api: DroniadaApi,
    drop_position: PointToFlyTo,
    circle_color: str,
    drone_telem_data: DroneTelemData,
):
    """
    Full drop pesticides routine
    """
    pesticide_to_apply = _ROBUR_TO_PESTICIDE.get(circle_color, PesticideType.ORANGE)

    drop_command = CommandForDrone(
        command_type=CommandType.DROP_PESTICIDES,
        points_to_fly_to=[drop_position],
        pesticide_type=pesticide_to_apply,
    )

    api.current_command_for_drone = drop_command

    while not command_finished(drop_command, drone_telem_data):
        time.sleep(1)

    report_tree_of_life_drop_pesticide(
        lat=drop_position.lat,
        lon=drop_position.lon,
        pesticide=pesticide_to_apply,
        connection=api.mongo_connection,
    )


def report_tree_of_life_detection(
    lat: float,
    lon: float,
    pathogen_type: str,
    img_filename: str,
    connection: MongoConnection,
):
    try:
        response = requests.get(
            f"http://robur.agro.pl:9999/getImage?filename={img_filename}",
            timeout=20,
        )

        connection.send_message(
            MongoMessage(
                message_type=MongoMsgType.TOL_DETECT,
                msg_content_dict=MongoMessages.tol_detect(
                    pathogen_type=pathogen_type,
                    gps_latitude=lat,
                    gps_longitude=lon,
                    img=response.content,
                ),
            )
        )

    except Exception as e:
        logger.exception(e)


def report_tree_of_life_drop_pesticide(
    lat: float, lon: float, pesticide: PesticideType, connection: MongoConnection
):
    connection.send_message(
        MongoMessage(
            message_type=MongoMsgType.TOL_APPLY,
            msg_content_dict=MongoMessages.tol_apply(
                pesticides_type=pesticide.value,
                gps_latitude=lat,
                gps_longitude=lon,
            ),
        )
    )


def report_infected_tree(infected_tree, connection: MongoConnection):
    try:
        circle_color = infected_tree.detections[0].color
        filename = infected_tree.detections[0].filename
        pathogen_type = _ROBUR_TO_MONGO_COLOR.get(circle_color, "beige")
        lat, lon = infected_tree.average_position
        report_tree_of_life_detection(
            pathogen_type=pathogen_type,
            lat=lat,
            lon=lon,
            img_filename=filename,
            connection=connection,
        )
    except Exception as e:
        logger.exception(e)


def report_mission_end(connection: MongoConnection):
    logger.critical("Finishing mission")
    connection.send_message(
        MongoMessage(
            message_type=MongoMsgType.FINISH, msg_content_dict=MongoMessages.finish()
        )
    )
