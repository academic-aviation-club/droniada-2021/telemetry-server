import logging
from datetime import datetime
from os import environ

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

logger = logging.getLogger(__name__)

DEFAULT_URL = "http://localhost:8086"
DEFAULT_ORGANISATION = "AKL"
DEFAULT_BUCKET = "Droniada"


class InfluxTelemetrySender:
    """
    Sends telemetry in a non-blocking way
    via a separate thread doing the requests
    """

    def __init__(
        self,
        token: str,
        url=DEFAULT_URL,
        bucket=DEFAULT_BUCKET,
        organisation=DEFAULT_ORGANISATION,
    ):
        self.client = InfluxDBClient(url, token)
        self.organisation = organisation
        self.bucket = bucket

        # TODO: figure out how ASYNCHRONOUS write option works,
        # unfortunately there is almost zero documentation on it
        self.write_api = self.client.write_api(write_options=SYNCHRONOUS)

    def send_telemetry(self, packet: dict):

        if "timestamp" not in packet:
            logger.error("No timestamp in packet: %s, discarding", str(packet))
            return

        if "drone_name" not in packet:
            logger.error("No drone_name in packet: %s, discarding", str(packet))
            return

        timestamp = datetime.fromtimestamp(packet["timestamp"])
        drone_name = packet["drone_name"]

        del packet["timestamp"]
        del packet["drone_name"]

        logger.debug("Sending %s %s", str(timestamp), str(packet))

        datapoint_to_send = Point(drone_name)
        datapoint_to_send.time(timestamp, WritePrecision.MS)

        for key, value in packet.items():
            datapoint_to_send.field(key, value)

        self.write_api.write(self.bucket, self.organisation, datapoint_to_send)


if __name__ == "__main__":
    telemetry_sender = InfluxTelemetrySender(environ["INFLUX_TOKEN"])
    telemetry_sender.send_telemetry(
        {
            "drone_name": "jebacpis",
            "timestamp": datetime.timestamp(datetime.utcnow()),
            "north": 123,
            "south": 123,
            "whatever": 12.123,
            "asdddd": 123,
        }
    )
